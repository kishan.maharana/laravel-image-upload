<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\image;
class uploadController extends Controller
{
    public function store(Request $request)
    {
    	$image=new image;
        if($request->hasfile('image'))
        {
            $file=$request->file('image');
            $extension=$file->getClientOriginalExtension();
            $filename=time().'.'.$extension;
            $file->move('public/upload/userimg/',$filename);
            $image->image=$filename;
        }
        else
        {
            return $request;
            $image->image='';
        }
		$image->save();
		return view('welcome');
    }
}
